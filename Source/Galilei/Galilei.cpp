// Copyright Epic Games, Inc. All Rights Reserved.

#include "Galilei.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Galilei, "Galilei");
