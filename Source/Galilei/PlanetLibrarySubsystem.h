// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlanetInfo.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "PlanetLibrarySubsystem.generated.h"

UENUM()
enum class EPlanetSortingRules : uint8
{
	Default UMETA(DisplayName = "Planet ID"),
	PlanetName UMETA(DisplayName = "Planet Name"),
	PlanetHost UMETA(DisplayName = "Planet Host"),
	PlanetCount UMETA(DisplayName = "Planet Count"),
	StarCount UMETA(DisplayName = "Star Count"),
	DiscoveryYear UMETA(DisplayName = "Discovery Year"),
	DiscoveryMethod UMETA(DisplayName = "Discovery Method"),
	ReleaseDate UMETA(DisplayName = "Release Date"),
};

/**
 * Class to manage planets references
 */
UCLASS(Blueprintable)
class GALILEI_API UPlanetLibrarySubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

	UPROPERTY(Transient, BlueprintGetter=GetPlanets)
	TArray<UPlanetInfo*> Planets;

	UFUNCTION(BlueprintGetter)
	TArray<UPlanetInfo*> GetPlanets() const { return Planets; }

	UFUNCTION(BlueprintCallable)
	void SortPlanets(EPlanetSortingRules SortingRule);

	UFUNCTION(BlueprintCallable)
	void AddPlanet(FPlanetInfoStruct InPlanetInfo);
};
