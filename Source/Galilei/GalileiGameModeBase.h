// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Engine/DataTable.h"
#include "Templates/SharedPointer.h"
#include "MessageEndpoint.h"
#include "PlanetInfo.h"
#include "GalileiGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdatePlanetInfoGeneratorThreadsSignature, FPlanetInfoStruct, StarInfo);

USTRUCT(BlueprintType, Atomic)
struct FBusStructMessage_PlanetInfo
{
	GENERATED_BODY()

	FPlanetInfoStruct PlanetInfo;
	FBusStructMessage_PlanetInfo(const FPlanetInfoStruct& InPlanetInfo = FPlanetInfoStruct())
		: PlanetInfo(InPlanetInfo) {}
};

/**
 *
 */
UCLASS()
class GALILEI_API AGalileiGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	TArray<UDataTable*> PlanetInfoChunks;

	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable)
	void RunDataReaders();

	UPROPERTY(BlueprintAssignable)
	FOnUpdatePlanetInfoGeneratorThreadsSignature OnUpdateStarInfoGenerator;

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndpoint_PlanetInfoGenerator;

	void BusMessageHandler_PlanetInfoGenerator(const FBusStructMessage_PlanetInfo& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void EventMessage_PlanetInfoGenerator(const FPlanetInfoStruct& InPlanetInfo);

	TArray<FRunnableThread*> DataReaderThreads;
};
