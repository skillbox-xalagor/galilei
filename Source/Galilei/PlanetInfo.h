#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "PlanetInfo.generated.h"

USTRUCT(BlueprintType, meta = (HiddenByDefault))
struct FPlanetInfoStruct : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planet ID"))
	int loc_rowid = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planet Name"))
	FString pl_name = "88 Xxx a";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Host Name"))
	FString hostname = "88 Xxx";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Default Parameter Set"))
	bool default_flag = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Number of Stars"))
	int32 sy_snum = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Number of Planets"))
	int32 sy_pnum = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Discovery Method"))
	FString discoverymethod = "Imaging";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Discovery Year"))
	int32 disc_year = 2000;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Discovery Facility"))
	FString disc_facility = "Lorem Observatory";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Solution Type"))
	FString soltype = "Published Confirmed";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Controversial Flag"))
	bool pl_controv_flag = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planetary Parameter Reference"))
	FString pl_refname = "<a refstr=LOREM_IPSUM href=https://example.com target=ref> Lorem Ipsum</a>";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Orbital Period in days"))
	float pl_orbper = 888.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Orbit Semi-Major Axis"))
	float pl_orbsmax = 8.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planet Earth Radius"))
	float pl_rade = 8.888;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planet Jupiter Radius"))
	float pl_radj = 8.888;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planet Earth Mass or Mass*sin(i)"))
	float pl_bmasse = 8888.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planet Jupiter Mass or Mass*sin(i)"))
	float pl_bmassj = 88.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planet Mass or Mass*sin(i)"))
	FString pl_bmassprov = "Mass";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Eccentricity"))
	float pl_orbeccen = 8.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Insolation Flux"))
	float pl_insol = 8888.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Equilibrium Temperature"))
	float pl_eqt = 8888;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Data show Transit Timing Variations"))
	bool ttv_flag = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Stellar Parameter Reference"))
	FString st_refname = "<a refstr=LOREM_IPSUM href=https://example.com target=ref> Lorem Ipsum</a>";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Spectral Type"))
	FString st_spectype = "XX III";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Stellar Effective Temperature"))
	float st_teff = 8888;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Stellar Radius"))
	float st_rad = 88.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Stellar Mass"))
	float st_mass = 8.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Stellar Metallicity"))
	float st_met = 8.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Stellar Metallicity Ratio"))
	FString st_metratio = "[Fe/H]";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Stellar Surface Gravity"))
	float st_logg = 8.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "System Parameter Reference"))
	FString sy_refname = "<a refstr=LOREM_IPSUM href=https://example.com target=ref> Lorem Ipsum</a>";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "RA sexagesimal"))
	FString rastr = "88h88m88.88s";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "RA deg"))
	float ra = 888.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Dec sexagesimal"))
	FString decstr = "+88h88m88.88s";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Dec deg"))
	float dec = 88.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Distance pc"))
	float sy_dist = 88.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "V Johnson Magnitude"))
	float sy_vmag = 8.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Ks 2MASS Magnitude"))
	float sy_kmag = 8.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Gaia Magnitude"))
	float sy_gaiamag = 8.88;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Date of Last Update"))
	FString rowupdate = "2000-01-01";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Planetary Parameter Reference Publication Date"))
	FString pl_pubdate = "2000-01-01";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Release Date"))
	FString releasedate = "2000-01-01";

	//FStarInfo() {}
};

UCLASS(Blueprintable)
class GALILEI_API UPlanetInfo : public UObject
{
	GENERATED_BODY()

public:
	// Sets default values for this empty's properties
	UPlanetInfo();

	UPROPERTY(BlueprintReadWrite, meta=(ExposeOnSpawn=true))
	FPlanetInfoStruct PlanetInfoData;
};
