// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanetLibrarySubsystem.h"

void UPlanetLibrarySubsystem::SortPlanets(const EPlanetSortingRules SortingRule)
{
	switch (SortingRule)
	{
		case EPlanetSortingRules::DiscoveryMethod:
			Planets.Sort([&](const UPlanetInfo& LHS, const UPlanetInfo& RHS) {
				return (RHS.PlanetInfoData.discoverymethod.Compare(LHS.PlanetInfoData.discoverymethod) == 1);
			});
			break;
		case EPlanetSortingRules::DiscoveryYear:
			Planets.Sort([&](const UPlanetInfo& LHS, const UPlanetInfo& RHS) {
				return (RHS.PlanetInfoData.disc_year > LHS.PlanetInfoData.disc_year);
			});
			break;
		case EPlanetSortingRules::PlanetCount:
			Planets.Sort([&](const UPlanetInfo& LHS, const UPlanetInfo& RHS) {
				return (RHS.PlanetInfoData.sy_pnum > LHS.PlanetInfoData.sy_pnum);
			});
			break;
		case EPlanetSortingRules::PlanetHost:
			Planets.Sort([&](const UPlanetInfo& LHS, const UPlanetInfo& RHS) {
				return (RHS.PlanetInfoData.hostname.Compare(LHS.PlanetInfoData.hostname) == 1);
			});
			break;
		case EPlanetSortingRules::PlanetName:
			Planets.Sort([&](const UPlanetInfo& LHS, const UPlanetInfo& RHS) {
				return (RHS.PlanetInfoData.pl_name.Compare(LHS.PlanetInfoData.pl_name) == 1);
			});
			break;
		case EPlanetSortingRules::ReleaseDate:
			Planets.Sort([&](const UPlanetInfo& LHS, const UPlanetInfo& RHS) {
				return (RHS.PlanetInfoData.releasedate.Compare(LHS.PlanetInfoData.releasedate) == 1);
			});
			break;
		case EPlanetSortingRules::StarCount:
			Planets.Sort([&](const UPlanetInfo& LHS, const UPlanetInfo& RHS) {
				return (RHS.PlanetInfoData.sy_snum > LHS.PlanetInfoData.sy_snum);
			});
			break;
		default:
			Planets.Sort([&](const UPlanetInfo& LHS, const UPlanetInfo& RHS) {
				return (RHS.PlanetInfoData.loc_rowid > LHS.PlanetInfoData.loc_rowid);
			});
			break;
	}
}

void UPlanetLibrarySubsystem::AddPlanet(const FPlanetInfoStruct InPlanetInfo)
{
	const auto NewPlanet = NewObject<UPlanetInfo>();
	NewPlanet->PlanetInfoData = InPlanetInfo;
	Planets.Add(NewPlanet);
}
