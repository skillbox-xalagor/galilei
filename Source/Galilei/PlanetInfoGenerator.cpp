// Fill out your copyright notice in the Description page of Project Settings.

#include "PlanetInfoGenerator.h"
#include "MessageEndpointBuilder.h"
#include "GalileiGameModeBase.h"

FPlanetInfoGenerator::FPlanetInfoGenerator(FColor Color, AGalileiGameModeBase* OwnerActor, UDataTable* InTable)
{
	ThreadColor = Color;
	GameMode_Ref = OwnerActor;
	LoadedTable = InTable;

	// IMessageBus sender init
	SenderEndpoint = FMessageEndpoint::Builder("Sender_FPlanetInfoGenerator").Build();
}

FPlanetInfoGenerator::~FPlanetInfoGenerator()
{
}

uint32 FPlanetInfoGenerator::Run()
{
	SenderEndpoint = FMessageEndpoint::Builder("Sender_FStarInfoGenerator").Build();

	//check(LoadedTable->GetRowStruct()->IsChildOf(FStarInfo::StaticStruct()));

	for (const TPair<FName, uint8*>& RowItr : LoadedTable->GetRowMap())
	{
		const FPlanetInfoStruct* LoadedStarInfo = reinterpret_cast<const FPlanetInfoStruct*>(RowItr.Value);
		if (SenderEndpoint.IsValid())
			SenderEndpoint->Publish<FBusStructMessage_PlanetInfo>(new FBusStructMessage_PlanetInfo(*LoadedStarInfo));
	}

	return 0;
}

void FPlanetInfoGenerator::Stop()
{
	bIsStopStarInfoGenerator = true;
}

void FPlanetInfoGenerator::Exit()
{
	if (SenderEndpoint.IsValid())
		SenderEndpoint.Reset();

	GameMode_Ref = nullptr;
}