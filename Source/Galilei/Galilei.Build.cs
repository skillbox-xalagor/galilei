// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Galilei : ModuleRules
{
	public Galilei(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG", "CommonUI" });
	}
}
