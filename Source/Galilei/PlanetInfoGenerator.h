// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MessageEndpoint.h"
#include "Engine/DataTable.h"

class AGalileiGameModeBase;

/**
* 
*/
class GALILEI_API FPlanetInfoGenerator : public FRunnable
{
	public:
	FPlanetInfoGenerator(FColor Color, AGalileiGameModeBase* OwnerActor, UDataTable* InTable);
	virtual ~FPlanetInfoGenerator() override;

	FColor ThreadColor;
	AGalileiGameModeBase *GameMode_Ref = nullptr;
	UDataTable* LoadedTable = nullptr;
	FThreadSafeBool bIsStopStarInfoGenerator = false;
	
	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderEndpoint;
};