// Copyright Epic Games, Inc. All Rights Reserved.

#include "GalileiGameModeBase.h"
#include "MessageEndpointBuilder.h"
#include "PlanetInfoGenerator.h"

void AGalileiGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	ReceiveEndpoint_PlanetInfoGenerator = FMessageEndpoint::Builder("Receiver_AGalileiGameModeBase").Handling<FBusStructMessage_PlanetInfo>(this, &AGalileiGameModeBase::BusMessageHandler_PlanetInfoGenerator);

	if (ReceiveEndpoint_PlanetInfoGenerator.IsValid())
	{
		ReceiveEndpoint_PlanetInfoGenerator->Subscribe<FBusStructMessage_PlanetInfo>();
	}
}

void AGalileiGameModeBase::RunDataReaders()
{
	for (const auto LoadedTable : PlanetInfoChunks)
	{
		class FPlanetInfoGenerator* MyStarInfoGenerator = new FPlanetInfoGenerator(FColor::Purple, this, LoadedTable);
		DataReaderThreads.Add(FRunnableThread::Create(MyStarInfoGenerator, TEXT("StarInfoReader Thread"), 0, EThreadPriority::TPri_Normal));
	}
}

void AGalileiGameModeBase::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (ReceiveEndpoint_PlanetInfoGenerator.IsValid())
	{
		ReceiveEndpoint_PlanetInfoGenerator.Reset();
	}
}

void AGalileiGameModeBase::BusMessageHandler_PlanetInfoGenerator(const FBusStructMessage_PlanetInfo& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_PlanetInfoGenerator(Message.PlanetInfo);
}

void AGalileiGameModeBase::EventMessage_PlanetInfoGenerator(const FPlanetInfoStruct& InPlanetInfo)
{
	OnUpdateStarInfoGenerator.Broadcast(InPlanetInfo);
}
