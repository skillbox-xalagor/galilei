﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GalileiGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class GALILEI_API UGalileiGameInstance : public UGameInstance
{
	GENERATED_BODY()

	virtual void Init() override;
};
